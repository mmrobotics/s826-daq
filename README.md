**Contributors**:

 - Adam Sperry

---
## Sensoray 826 (s826) PCIe Data Acquisition Card

The s826 is a data acquisition card that plugs into an open PCIe slot on your
computer and enables digital and analog I/O. The card is most notably used to
control the untethered magnetic haptic device but has been used elsewhere in
our lab (e.g. with the cochlear implant system).

---
## Repository Contents

This repository contains all the documentation and source code necessary to set
up an s826 card on a Linux or Windows machine. The repository contains a modified
version of the Linux SDK to fix various problems with the original Linux SDK. The
contents of the repository are as follows:

 - `Documentation/` - Contains the documentation for the s826 card and its
                      breakout boards.
 - `sdk_826_linux_3.3.15/` - The s826 Linux SDK source code. Contains source code
                             for both the kernel-level driver and the user-level
                             driver (referred to as the API Library).
 - `install_s826_kernel.sh` - A custom shell script used to set up and install the
                              kernel-level driver on Linux. This script replaces the
                              original kernel-level driver installation instructions
                              found in `sdk_826_linux_3.3.15/README`.
 - `uninstall_s826_kernel.sh` - A custom shell script that reverses the installation
                                steps performed by `install_s826_kernel.sh`
 - `dkms.conf` - The DKMS configuration file used to set up the kernel-level driver
                 with DKMS on Linux (described below).
 - `CMakeLists.txt` and `cmake/` - The Linux user-level driver (or API library) has
                                   been made into a CMake project for convenience
                                   when building and installing the library and when
                                   linking to the library from your own projects.
 - `sdk_826_linux_3.3.15.tar.bz2` - An archive file containing the original s826 Linux
                                    SDK source code. This archive file is present only
                                    to retain a copy of the original SDK in the event
                                    a modification to the source code is needed.
 - `sdk_826_win_3.4.11.zip` - The s826 Windows SDK. This repository is primarily directed
                              toward using the s826 on Linux. If you so desire, this
                              ZIP file contains the setup executable to install the
                              drivers on Windows (presumably both the kernel-level
                              and user-level drivers).
 - `sdk_826_matlab_1.0.0.zip` - This ZIP file contains an API wrapper for MATLAB so
                                the s826 can be used from MATLAB. I believe this only
                                works on Windows and requires the SDK for Windows to
                                be installed.
 - `sdk_826_labview_1.0.0.zip` - This ZIP file contains files to use the s826 with
                                 LabVIEW.

---
## Documentation
The `Documentation/` directory contains copies of all the documentation offered
by Sensoray for the s826 on its
[product page](http://www.sensoray.com/products/826.htm). There are some wiki
pages that are not in the `Documentation/` folder that can be found on the Sensoray
website. The [technical support wiki](http://www.sensoray.com/wiki/index.php?title=826)
is of particular interest. Make sure to read `Documentation/Technical_Manual.pdf` at
a minimum.

If the Sensoray 826 ever becomes a legacy product, the content from the wiki pages
should be downloaded and stored in this repository to preserve the information in
case it is removed from the internet.

---
## Installation and Usage on Linux

The s826 driver consists of two parts: the kernel-level driver and the user-level
driver (referred to as the API library). The process of using an s826 card on
Linux involves installing the kernel-level driver, compiling the API library,
then linking to the API library in your own source code so you can interact with
the card using the s826 API functions (see `Documentation/Technical_Manual.pdf`
for API function documentation). This repository contains some modifications from
the original SDK to fix some issues and make setup/usage easy. Please follow the
instructions below to set up and use the s826 on Linux. For reference,
`sdk_826_linux_3.3.15/README` contains the original driver setup instructions from
Sensoray, but again, the instructions below are intended to replace the instructions
in `sdk_826_linux_3.3.15/README`.

### Installing the Kernel-Level Driver

#### A Note on Secure Boot

If the following instructions do not work, you may need to disable Secure Boot from
the EFI BIOS on your computer. Just hold the delete key or whatever key is necessary
to enter your EFI BIOS settings when the computer first boots up. With Secure Boot
on, you won't be able to load kernel modules (drivers) from 3rd party vendors that
are not signed with a special key. This s826 driver is not normally signed with the
special key, however, DKMS (explained below) should sign the s826 driver with a key
so it should not be necessary to disable Secure Boot if you follow the instructions
below. Kernel module key signing has recently started being enforced in Ubuntu and
is something to be aware of with modern Ubuntu.

#### Installation Instructions

Make sure that you have installed `build-essential` and `dkms` with the following
command:

    sudo apt install build-essential dkms

The `install_s826_kernel.sh` script is intended to make the kernel-level driver
installation simple. It will set up the driver source code for compilation and
installation using DKMS. Simply run the script with root privileges and read the
output carefully to verify that the installation was successful.

    cd s826-daq
    sudo ./install_s826_kernel.sh

As a reminder, this install script is intended to replace the original installation
methods described in `sdk_826_linux_3.3.15/README`.

#### Verifying the Installation

There are several terminal commands that can be used to display info about kernel
level drivers:

 - `lsmod` - lists all the drivers that are currently loaded. The name of the s826
             driver is `s826`.
 - `modinfo <driver name>` - used to get information about a particular driver.

After the installation script has finished, use `modinfo s826` to verify that the
driver is installed.

To verify that the driver is loaded (i.e. your computer has identified that an s826
card is present) verify that "s826" is in the output of `lsmod`. This can easily be
done using `lsmod | grep s826` and checking that the output says "s826". If there is
no output, you may need to reboot the computer.

#### Description of Kernel-Level Driver Fixes

The problems with the original kernel-level driver installation process are as follows:

 - The kernel-level driver installation does not carry over when the Linux kernel
   is updated (which happens frequently when the OS is updated). This means the
   driver may need to be manually installed again if the OS is updated.

To fix the issue with OS updates, the `install_s826_kernel.sh` script sets up the
driver to be built and installed using DKMS. DKMS (Dynamic Kernel Module Support)
is very useful as it will register the driver to be reinstalled automatically every
time the kernel is updated. A good resource to learn more about DKMS and how it
works is to read the manual by typing `man dkms` into the terminal.

For further understanding, study up on the following topics: `udev`, `sysfs`,
`/dev` directory, `devtempfs`, `devfs`, `systemd`, module commands (`lsmod`,
`modinfo`, `insmod`, `rmmod`, `modprobe`), and general Linux driver development.

### Compiling and Using the API Library (User-Level Driver)

Once the kernel-level driver has been installed, the API library (User-Level
Driver) needs to be compiled and linked to your project. For convenience, a CMake
project has been created to build and install the library, making it easy to use in
your own project. Alternatively, you can directly use Sensoray's Makefile
`sdk_826_linux_3.3.15/Makefile` to build the API library (see instructions below).
However, it is recommended that you build and install the library using CMake.

#### Building and Installing with CMake

To build and install the API library with CMake, simply execute the following
commands from the top level of this repository (e.g. `cd s826-daq`):

```
mkdir build
cd build
cmake ../
sudo make install
```

These commands create a `build` directory at the top level of the repository
and execute the build from inside that directory. They then install the library
into `/usr/local/` (the default installation path for CMake). This location makes
the library available to any user on the computer, but requires `sudo` privileges.
If desired, the installation path can be changed by setting the
`CMAKE_INSTALL_PREFIX` CMake variable as follows:

```
mkdir build
cd build
cmake ../ -DCMAKE_INSTALL_PREFIX=/home/john/packages/s826
make install
```

The above commands will install into `/home/john/packages/s826` which does not
require `sudo` privileges. However, be aware that if you chose to install in a custom
location, you will need to understand how to use the `CMAKE_PREFIX_PATH` CMake
variable to let CMake know where the API library is located when building projects
that use the library. Therefore, it is recommended to stick with the default location
of `/usr/local/`.

To link against the API library, use `find_package()` and `target_link_libraries()`
in your `CMakeLists.txt` file:

```
find_package(s826 REQUIRED)

# Create a CMake target called "myTarget"

target_link_libraries(myTarget s826::s826)
```

#### Building with Sensoray's Makefile

To compile simply call `make lib` from inside the SDK source directory
`sdk_826_linux_3.3.15/`

    cd sdk_826_linux_3.3.15
    make lib

This will create the static library `lib826_64.a` in the source directory.

Linking against the library is done in your preferred way. For linking with
compiler flags:

 - Make sure the directory where the library resides is included in the
   search using the `-L` compiler flag
 - Use the `-l826_64` compiler flag when linking


#### Including the API in source code

Include the API in source code using:

    extern "C" {
	    #include "826api.h"
    }

This will make the API functions available to use. See
`Documentation/Technical_Manual.pdf` for API function documentation.

Note that if you built using Sensoray's Makefile, you will need to make sure the SDK
source directory is in your list of include directories (e.g. using the `-I`
compiler flag).
