#! /usr/bin/env bash

# Author: Adam Sperry

# This script is meant to reverse the steps taken by install_s826_kernel.sh
# to uninstall the s826 kernel level driver.

# Helper functions
msg() {
    echo >&2 -e "${1-}"
}

die() {
    local msg=$1
    local code=${2-1} # default exit status 1
    msg "$msg"
    exit "$code"
}

# Make sure the user wants to uninstall the kernel level driver
echo
echo "This script will uninstall the s826 kernel level driver!"
read -p "Are you sure you want to do this [y/N]? " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    die "Uninstallation aborted"
fi

# Make sure that DKMS has been installed
if ! command -v dkms &> /dev/null; then
    die "DKMS must be installed before running this script. (e.g. sudo apt install dkms)"
fi

# Make sure the script has been called as root
if [ "$(id -u)" != "0" ]; then
    die "This script needs to be run as root. (e.g. sudo ./uninstall_s826_kernel.sh)"
fi

# Get the absolute path to this script
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Specify the installed source directory
installDir=/usr/src/s826-1.0.8

# Remove the module from DKMS
echo "Removing the module from DKMS...";
if ! dkms remove -m s826 -v 1.0.8 --all; then
    echo "Could not remove the module."
fi

# Remove the driver source files from the installed location
echo "Removing installed source files from $installDir...";
if [[ -d $installDir ]]; then
    rm -rf $installDir
else
    echo "Source files were not present."
fi

# Remove the udev rule
echo "Removing Udev rule...";
if [[ -f /etc/udev/rules.d/10-local-826.rules ]]; then
    rm -f /etc/udev/rules.d/10-local-826.rules
else
    echo "Udev rule was not present."
fi
